public class arrayList<E> {
    customArray<E> arrList = new customArray<>();
    public void add(E value)throws arrayException{
        if (value==null){
            throw new arrayException("Inputted null value");
        }else if (arrList.contains(value)){
            throw new  arrayException("Duplicate value : "+value);
        }else
            arrList.add(value);
    }

    public E get(int i){
        return arrList.get(i);
    }

    public int size(){
        return arrList.size();
    }
}
