import java.util.Arrays;

public class customArray<E> {
    private static final int INITIAL_CAPACITY = 1000;
    private int size = 0;
    private Object elementData[] = {};

    public customArray() {
        elementData = new Object[INITIAL_CAPACITY];
    }

    public void add(E e)throws arrayException{
        if (size == elementData.length){
            ensureCapacity();
        }
        elementData[size++] = e;
    }

    public E get(int index) {
        // if index is negative or greater than size of size, we throw
        // Exception.
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size "
                    + index);
        }
        return (E) elementData[index]; // return value on index.
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.print(elementData[i] + "\n");
        }
    }

    public int indexOf(Object var1) {
        for(int i = 0; i < this.size; ++i) {
            if (var1.equals(this.elementData[i])) {
                return i;
            }
        }
        return -1;
    }

    public boolean contains(Object var1) {
        return this.indexOf(var1) >= 0;
    }


    private void ensureCapacity() {
        int newCapacity = elementData.length * 2;
        elementData = Arrays.copyOf(elementData, newCapacity);
    }

    public int size() {
        return size;
    }
}
